/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.mypackage.hello;

import java.util.Date;

/**
 *
 * @author Srikanta
 */
public class EventModel {
    int eID;
    String eName;
    String eDescription;
    Date eDate;
    int eTime;
    int eCapaity;

    public int geteID() {
        return eID;
    }

    public String geteName() {
        return eName;
    }

    public String geteDescription() {
        return eDescription;
    }

    public Date geteDate() {
        return eDate;
    }

    public int geteTime() {
        return eTime;
    }

    public int geteCapaity() {
        return eCapaity;
    }

    public void seteID(int eID) {
        this.eID = eID;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public void seteDescription(String eDescription) {
        this.eDescription = eDescription;
    }

    public void seteDate(Date eDate) {
        this.eDate = eDate;
    }

    public void seteTime(int eTime) {
        this.eTime = eTime;
    }

    public void seteCapaity(int eCapaity) {
        this.eCapaity = eCapaity;
    }
    
    
}
