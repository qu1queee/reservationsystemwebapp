<%-- 
    Document   : checkjsp
    Created on : 3/05/2014, 08:05:35 PM
    Author     : EnriqueEncalada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String name = "";
    if ( request.getParameter("name") != null ) {
        name = request.getParameter("name");
    }

    String password = "";
    if ( request.getParameter("password") != null ) {
        password = request.getParameter("password");
    }

    if ( ( name.equals("enrique") ) && ( password.equals("1234")) ) {
        out.println( "Welcome." );
        %>
    <html>
    <head>
        <title>JSP Web Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/affablebean.css">
    </head>
    <body>
        <div id="main"> 
            <div id="header">
                <div id="widgetBar">
                    <div class="headerWidget">
                        <form name="Name Input Form" action="checkjsp.jsp">
                        <input type="submit" value="LogOut" />
                    </form>
                    </div>
                    <div class="headerWidget">
                        [ Language ]
                    </div>
                </div>
                <a href="#">
                    <img src="#" id="logo" alt="Affable Bean logo">
                </a>
                <img src="#" id="logoText" alt="Reservation System">
            </div>
            
            <div id="indexLeftColumn">
                
            </div>
            <div id="indexRightColumn">
                         
            </div>
            
            <div id="footer">
                Privacy Policy :: Contact  @ 2014 Reservation System
            </div>
            
        </div>
    </body>
    </html>
<%
    } else {
        out.println( "Wrong credentials, please try again." );
       %> 
       <html>
    <head>
        <title>JSP Web Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/affablebean.css">
    </head>
    <body>
        <div id="main"> 
            <div id="header">
                <div id="widgetBar">
                    <div class="headerWidget">
                        [ Language ]
                    </div>
                </div>
                <a href="#">
                    <img src="#" id="logo" alt="Affable Bean logo">
                </a>
                <img src="#" id="logoText" alt="Reservation System">
            </div>
            
            <div id="indexLeftColumn">
                
            </div>
            <div id="indexRightColumn">
                
                <div class="categoryBox">
                    <form name="Name Input Form" action="checkjsp.jsp">
                        Name: <input type="text" name="name"/><br>
                        Pass : <input name="password" type="password"><br>
                        <input type="submit" value="Login" />
                    </form>
                    <form name="Password forgottem" action="forgotpassjsp.jsp">
                        <input type="submit" value="Forgot Password" />
                    </form>
                </div>             
            </div>
            
            <div id="footer">
                Privacy Policy :: Contact  @ 2014 Reservation System
            </div>
            
        </div>
    </body>
</html>
       
<%
    }
%>

