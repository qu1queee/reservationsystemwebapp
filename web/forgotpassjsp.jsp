<%-- 
    Document   : forgotpassjsp
    Created on : 3/05/2014, 08:08:57 PM
    Author     : EnriqueEncalada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
out.println( "A provisional password has been send to your email address" );
%>
<html>
    <head>
        <title>JSP Web Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/affablebean.css">
    </head>
    <body>
        <div id="main"> 
            <div id="header">
                <div id="widgetBar">
                    <div class="headerWidget">
                        [ Language ]
                    </div>
                </div>
                <a href="#">
                    <img src="#" id="logo" alt="Affable Bean logo">
                </a>
                <img src="#" id="logoText" alt="Reservation System">
            </div>
            
            <div id="indexLeftColumn">
             </div>
            <div id="indexRightColumn">
                <div class="categoryBox">
                    <form name="Name Input Form" action="checkjsp.jsp">
                        Email: <input type="text" name="name"/><br>
                        <input type="submit" value="Submit" />
                    </form>
                </div>                
            </div>
            
            <div id="footer">
                Privacy Policy :: Contact  @ 2014 Reservation System
            </div>
            
        </div>
    </body>
</html>
