<%-- 
    Document   : availableeventsjsp
    Created on : 3/05/2014, 08:31:41 PM
    Author     : Srikanta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Available Events Page</title>
        <%@include file="includes/header.jsp" %>
    </head>
    <body>
        <div id="main"> 

            <!-- Start of Header -->
             <%@include file="includes/bodytop.jsp" %>
            <!-- End of Header -->
            
             <!-- Beginning of Body -->
            <div id="indexLeftRightColumn">
                <div id="tabs">
                    <ul>
                      <li><a href="#fragment-1"><span>Available Events</span></a></li>
                      <li><a href="#fragment-2"><span>Registered Events</span></a></li>
                    </ul>
                    <div id="fragment-1">
                        First Tab
                    </div>
                    <div id="fragment-2">
                    Second Tab
                    </div>
                </div>
               
                
            </div>
            
            <!-- Beginning of footer -->
            <div id="footer">
                Privacy Policy :: Contact  @ 2014 Reservation System
            </div>
            <!-- End of footer -->
            
        </div>
        <script>
             $( "#tabs" ).tabs();
        </script>
    </body>
