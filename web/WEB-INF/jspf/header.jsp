<%-- 
    Document   : header
    Created on : 3/05/2014, 07:08:12 PM
    Author     : EnriqueEncalada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Web Page</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/affablebean.css">
    </head>
    <body>
        <div id="main"> 
            <div id="header">
                <div id="widgetBar">
                    <div class="headerWidget">
                        [ Log Out ]
                    </div>
                    <div class="headerWidget">
                        [ Language ]
                    </div>
                </div>
                <a href="#">
                    <img src="#" id="logo" alt="Affable Bean logo">
                </a>
                <img src="#" id="logoText" alt="Reservation System">
            </div>
