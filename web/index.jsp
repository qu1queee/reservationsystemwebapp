<%-- 
    Document   : index
    Created on : 4 May, 2014, 1:42:20 PM
    Author     : Srikanta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP Web Page</title>
        <%@include file="includes/header.jsp" %>
    </head>
    <body>
        <div id="main"> 

            <!-- Start of Header -->
             <%@include file="includes/bodytop.jsp" %>
            <!-- End of Header -->
            
             <!-- Beginning of Body -->
            <div id="indexLeftColumn">
                
            </div>
            
            <div id="indexRightColumn">
                <div class="categoryBox">
                    <form name="Name Input Form" action="checkjsp.jsp">
                        Name: <input type="text" name="name"/><br>
                        Pass : <input name="password" type="password"><br>
                        <input type="submit" value="Login" />
                    </form>
                    <form name="Password forgottem" action="forgotpassjsp.jsp">
                        <input type="submit" value="Forgot Password" />
                    </form>
                </div>             
            </div>
            
            <!-- Beginning of footer -->
            <div id="footer">
                Privacy Policy :: Contact  @ 2014 Reservation System
            </div>
            <!-- End of footer -->
            
        </div>
    </body>
